from PyQt5 import QtCore, QtGui, QtWidgets
import random

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(325, 205)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout.addWidget(self.lineEdit, 0, 0, 1, 3)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 3, 1, 1)
        self.spinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.spinBox.setMinimum(8)
        self.spinBox.setObjectName("spinBox")
        self.gridLayout.addWidget(self.spinBox, 1, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 3, 1, 1)
        self.radioButton_4 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_4.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.radioButton_4.setChecked(True)
        self.radioButton_4.setObjectName("radioButton_4")
        self.gridLayout.addWidget(self.radioButton_4, 2, 0, 1, 1)
        self.radioButton_2 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_2.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.radioButton_2.setObjectName("radioButton_2")
        self.gridLayout.addWidget(self.radioButton_2, 2, 1, 1, 1)
        self.radioButton_3 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_3.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.radioButton_3.setChecked(False)
        self.radioButton_3.setObjectName("radioButton_3")
        self.gridLayout.addWidget(self.radioButton_3, 2, 2, 1, 1)
        self.radioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.radioButton.setChecked(False)
        self.radioButton.setObjectName("radioButton")
        self.gridLayout.addWidget(self.radioButton, 2, 3, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 3, 0, 1, 4)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 325, 28))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.pushButton.clicked.connect(self.creat)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PassWord Generator"))
        self.label_2.setText(_translate("MainWindow", "پسورد :"))
        self.label.setText(_translate("MainWindow", "تعداد کاراکتر :"))
        self.radioButton_4.setText(_translate("MainWindow", "مخلوط"))
        self.radioButton_2.setText(_translate("MainWindow", "اعداد"))
        self.radioButton_3.setText(_translate("MainWindow", "حروف کوچک"))
        self.radioButton.setText(_translate("MainWindow", "حروف بزرگ"))
        self.pushButton.setText(_translate("MainWindow", "ساختن"))
    def creat(self):
        if self.radioButton.isChecked() == True:
            number = self.spinBox.value()
            password = [random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ') for _ in range(number)]
            password = ''.join(password)
            self.lineEdit.setText(password)
            print(password)

        if self.radioButton_2.isChecked() == True:
            number = self.spinBox.value()
            password = [random.choice('12345678910') for _ in range(number)]
            password = ''.join(password)
            self.lineEdit.setText(password)
            print(password)

        if self.radioButton_3.isChecked() == True:
            number = self.spinBox.value()
            password = [random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(number)]
            password = ''.join(password)
            self.lineEdit.setText(password)
            print(password)

        if self.radioButton_4.isChecked()==True:
            number = self.spinBox.value()
            password = [random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678910abcdefghijklmnopqrstuvwxyz') for _ in range(number)]
            password=''.join(password)
            self.lineEdit.setText(password)
            print(password)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
